/* AUTO-GENERATED FILE.  DO NOT MODIFY.
 *
 * This class was automatically generated by the
 * gradle plugin from the resource data it found. It
 * should not be modified by hand.
 */
package me.dm7.barcodescanner.core;

public final class R {
    public static final class attr {
        public static int borderAlpha = 0x7f01000a;
        public static int borderColor = 0x7f010003;
        public static int borderLength = 0x7f010006;
        public static int borderWidth = 0x7f010005;
        public static int cornerRadius = 0x7f010008;
        public static int finderOffset = 0x7f01000b;
        public static int laserColor = 0x7f010002;
        public static int laserEnabled = 0x7f010001;
        public static int maskColor = 0x7f010004;
        public static int roundedCorner = 0x7f010007;
        public static int shouldScaleToFill = 0x7f010000;
        public static int squaredFinder = 0x7f010009;
    }
    public static final class color {
        public static int viewfinder_border = 0x7f030000;
        public static int viewfinder_laser = 0x7f030001;
        public static int viewfinder_mask = 0x7f030002;
    }
    public static final class integer {
        public static int viewfinder_border_length = 0x7f020000;
        public static int viewfinder_border_width = 0x7f020001;
    }
    public static final class styleable {
        public static int[] BarcodeScannerView = { 0x7f010000, 0x7f010001, 0x7f010002, 0x7f010003, 0x7f010004, 0x7f010005, 0x7f010006, 0x7f010007, 0x7f010008, 0x7f010009, 0x7f01000a, 0x7f01000b };
        public static int BarcodeScannerView_borderAlpha = 10;
        public static int BarcodeScannerView_borderColor = 3;
        public static int BarcodeScannerView_borderLength = 6;
        public static int BarcodeScannerView_borderWidth = 5;
        public static int BarcodeScannerView_cornerRadius = 8;
        public static int BarcodeScannerView_finderOffset = 11;
        public static int BarcodeScannerView_laserColor = 2;
        public static int BarcodeScannerView_laserEnabled = 1;
        public static int BarcodeScannerView_maskColor = 4;
        public static int BarcodeScannerView_roundedCorner = 7;
        public static int BarcodeScannerView_shouldScaleToFill = 0;
        public static int BarcodeScannerView_squaredFinder = 9;
    }
}
